function displayImages() {
    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");
    var spriteWidth = 80;
    var spriteHeight = 57;
    var spriteCols = 14;
    var spriteRows = 6;
    var img = new Image();
    img.onload = image;
    img.src = '/Users/susmitha-pt3822/Downloads/spritee.png';
    function image() {
        var row = document.getElementById("row").value;
        var col = document.getElementById("col").value;
        var len1 = row.length;
        var len2 = col.length;
        var canvasY = 0;
        for (var rowCtr = 0; rowCtr < spriteRows; rowCtr++) {
            for (var colCtr = 0; colCtr < spriteCols; colCtr++) {
                var sourceX = colCtr * spriteWidth;
                var sourceY = rowCtr * spriteHeight;
                var canvasX = 0;
                if (row == rowCtr && col == colCtr) {
                    context.drawImage(img,
                        sourceX, sourceY, spriteWidth, spriteHeight,
                        canvasX, canvasY, spriteWidth, spriteHeight
                    );
                }
                else if (colCtr == col && len1 == 0) {
                    canvasY = spriteHeight + 5 + canvasY;

                    context.drawImage(img,
                        sourceX, sourceY, spriteWidth, spriteHeight,
                        canvasX, canvasY, spriteWidth, spriteHeight
                    );
                }
                else if (rowCtr == row && len2 == 0) {
                    canvasY = spriteHeight + 5 + canvasY;
                    context.drawImage(img,
                        sourceX, sourceY, spriteWidth, spriteHeight,
                        canvasX, canvasY, spriteWidth, spriteHeight
                    );
                }

            }
        }
    }

}