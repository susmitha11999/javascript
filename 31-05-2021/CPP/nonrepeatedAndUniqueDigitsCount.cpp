#include <iostream>
using namespace std;

class Digits {
  public:
    int nonRepeatedDigitsCount(int value);
    int uniqueDigitsCount(int value);
};
int Digits::nonRepeatedDigitsCount(int value)
{
    int  rem=0,count=0,ctr;
    int arr[10]={0};
    while(value!=0)
    {
      rem =value%10;
      arr[rem]=arr[rem]+1;
      value=value/10;
    }
    for(ctr=0;ctr<10;ctr++)
   { 
      if(arr[ctr]==1)
      count++;
    }
return count;
}
int Digits::uniqueDigitsCount(int value)
{
    int  rem=0,count=0,ctr;
    int arr[10]={0};
    while(value!=0)
    {
      rem =value%10;
      if(arr[rem]==0)
      {
      arr[rem]=1;
      count++;
      }
      value=value/10;
    }
return count;
}
int main() {
    int input;
    cout<<"get the input: ";
    cin>>input;
    Digits obj;
    int result1=obj.nonRepeatedDigitsCount(input);
    int result2=obj.uniqueDigitsCount(input);
    cout<<"nonRepeatedDigitsCount:"<<result1;
    cout<<"\n"<<"uniqueDigitsCount:"<<result2<<"\n";
}

