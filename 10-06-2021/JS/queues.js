var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var moveBottomLeftTimer, moveBottomRightTimer;
var dequeTimer1, dequeTimer2, dequeTimer3, dequeTimer4;
var dequeRightFace = undefined;
var dequeLeftFace = undefined;
var x = 100;
var y = 100;
var botLeftFaceXPos = 175;
var botRightFaceXPos = 675;
var botFaceYPos = 450;
var dx = 2, dy = 1;
var leftFaces = [], rightFaces = [];
var k = 40;

for (i = 0; i < 5; i++) {
    let face = {
        x: 175,
        y: 350 - k * i
    };
    leftFaces.push(face);
}
for (i = 0; i < 5; i++) {
    let face = {
        x: 675,
        y: 350 - k * i
    };
    rightFaces.push(face);
}

function drawEllipse(x, y, color = "black") {
    ctx.beginPath();
    ctx.ellipse(x, y, 10, 15, 0, 0, Math.PI * 2);
    ctx.closePath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = color;
    ctx.stroke();
}

function drawLine(x, y, x1, y1) {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x1, y1);
    ctx.lineWidth = 2;
    ctx.strokeStyle = "black";
    ctx.stroke();
}

function drawBottomFaces() {
    drawEllipse(175, 450);
    drawEllipse(675, 450);
}

function drawBottomGates() {
    drawLine(100, 400, 250, 400);
    drawLine(600, 400, 750, 400);
}

function drawFaces(faces) {
    for (let i = 0; i < faces.length; i++) {
        let face = faces[i];
        drawEllipse(face.x, face.y);
    }
}

function main() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = 'white';
    ctx.fillRect(0,0,canvas.width,canvas.height);
    drawFaces(leftFaces);
    drawFaces(rightFaces);
    drawBottomGates();
    drawBottomFaces();
}

function clearRect(x, y, width, height, stroke) {
    ctx.clearRect(x - width - stroke, y - height - stroke, (width + stroke) * 2, (height + stroke) * 2);  
    ctx.fillStyle = 'white';
    ctx.fillRect(x - width - stroke, y - height - stroke, (width + stroke) * 2, (height + stroke) * 2);
}

function moveRed() {
    clearRect(x - dx, y, 10, 15, 2);

    drawEllipse(x, y, "#FF0000");
    if (x < 675) {
        x += dx;
    }
    else {
        clearInterval(moveTimer);
    }
}
function moveBottomLeft() {
    clearRect(botLeftFaceXPos,  botFaceYPos, 10, 15, 2);
    drawEllipse(botLeftFaceXPos,  botFaceYPos, "black");
    if ( botFaceYPos - 30 < canvas.height) {
        botFaceYPos += dy;
    }
    else {
        clearInterval(moveBottomLeftTimer);
    }

}
function moveBottomRight() {
    clearRect(botRightFaceXPos - dx, botFaceYPos, 10, 15, 2);
    botFaceYPos = 450;
    drawEllipse(botRightFaceXPos,  botFaceYPos);
    if (botRightFaceXPos - 15 < canvas.width) {
        botRightFaceXPos += dx;
    } else {
        clearInterval(moveBottomRightTimer);
    }
}

function dequeueLeft() {

    if (!dequeLeftFace) {
        clearInterval(dequeTimer3);
        return;
    }

    clearRect(dequeLeftFace.x + dx, dequeLeftFace.y, 10, 15, 2);
    drawEllipse(dequeLeftFace.x, dequeLeftFace.y);

    if (dequeLeftFace.x + 10 > 0) {
        dequeLeftFace.x -= dx;
    } else {
        clearInterval(dequeTimer3);
        delay();
    }

}

function dequeueRight() {

    if (!dequeRightFace) {
        clearInterval(dequeTimer4);
        return;
    }

    clearRect(dequeRightFace.x - dx, dequeRightFace.y, 10, 15, 2);
    drawEllipse(dequeRightFace.x, dequeRightFace.y);

    if (dequeRightFace.x - 10 < canvas.width) {
        dequeRightFace.x += dx;

    } else {
        clearInterval(dequeTimer4);
        delay1();

    }

}
function delay() {

    dequeTimer3 = setInterval(removeFromQueue, 2000);
    clearInterval(dequeTimer1);
    function removeFromQueue() {
        dequeLeftFace = leftFaces.shift();
        dequeTimer1 = setInterval(dequeueLeft, 5);
        check();
    }
}
function delay1() {

    dequeTimer4 = setInterval(removeFromRightQueue, 3500);
    clearInterval(dequeTimer2);
    function removeFromRightQueue() {
        dequeRightFace = rightFaces.shift();
        dequeTimer2 = setInterval(dequeueRight, 5);
        check();
    }
}
function check() {
    console.log(leftFaces.length);
    console.log(rightFaces.length);
    if (leftFaces.length < rightFaces.length) {
        var moveTimer1 = setInterval(move1, 5);

        function move1() {

            clearRect(x - dx + 4, y, 10, 15, 2);
            drawEllipse(x, y, "#FF0000");

            if (x > 175) {
                x -= dx;
            } else {
                clearInterval(moveTimer1);
            }
        }
    }
    else {

        var moveTimer2 = setInterval(move2, 5);

        function move2() {

            clearRect(x - dx, y, 10, 15, 2);
            drawEllipse(x, y, "#FF0000");

            if (x < 675) {
                x += dx;
            } else {
                clearInterval(moveTimer2);
            }
        }

    }

    if (leftFaces.length == 0) {
        moveBottomLeftTimer = setInterval(moveBottomLeft, 5);

    }
    if (rightFaces.length == 0) {
        moveBottomRightTimer = setInterval(moveBottomRight, 5);

    }

}
function startRecordingCanvas() {
    var dataArray=[];
    var canvas = document.getElementById('canvas');
    const vidstream = canvas.captureStream(); 
    const rec = new MediaRecorder(vidstream); 
      rec.ondataavailable = e => dataArray.push(e.data);
      rec.onstop = e => video(new Blob(dataArray, {type: 'video/webm'}));
      rec.start();
      setInterval(()=>rec.stop(), 27000); 
  
}

function video(blob) {
var vid = document.createElement('video');
  vid.src = URL.createObjectURL(blob);
  vid.controls = true;
  document.body.appendChild(vid);
console.log(vid);
  const a = document.createElement('a');
  a.download = 'queue.webm';
  a.href = vid.src;
  a.textContent = 'download the video';
  document.body.appendChild(a);
}

function startRecordingVideo() {
    var cameraButton = document.getElementById("start");
        var video = document.getElementById("video");
        var startButton = document.getElementById("startRec");
        var stopButton = document.getElementById("stopRec");
        var download = document.getElementById("download");
        var cameraStream = null;
        var recorder = null;
        var blobsArray = [];

        cameraButton.addEventListener('click', async function () {
            cameraStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
            video.srcObject = cameraStream;
        });

        startButton.addEventListener('click', function () {

            recorder = new MediaRecorder(cameraStream, { mimeType: 'video/webm' });


            recorder.addEventListener('dataavailable', function (e) {
                blobsArray.push(e.data);
            });


           recorder.addEventListener('stop', function () {

                var video_local = URL.createObjectURL(new Blob(blobsArray, { type: 'video/webm' }));
                download.href = video_local;
            });

            recorder.start(1000);
        });

        stopButton.addEventListener('click', function () {
            recorder.stop();
        });
}
main();
var moveTimer = setInterval(moveRed, 5);
var vidstream,audstream;
startRecordingCanvas();
startRecordingVideo();
delay();
delay1();
