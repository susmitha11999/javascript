var movesNum; 
var movescell = document.getElementById("movesnum");
var img1 = document.getElementsByClassName("Img1")[0];
var img2 = document.getElementsByClassName("Img2")[0];
var img3 = document.getElementsByClassName("Img3")[0];
var img4 = document.getElementsByClassName("Img4")[0];
var img5 = document.getElementsByClassName("Img5")[0];
var img6 = document.getElementsByClassName("Img6")[0];
var img7 = document.getElementsByClassName("Img7")[0];
var img8 = document.getElementsByClassName("Img8")[0];
var emptyRow, emptyCol;
var randomizePuzzle = function()  {
  movesNum = 0;
  movescell.innerHTML = movesNum; 
  [emptyRow, emptyCol] = [2, 2]; 
  var positions = [ [1,1], [1,2], [1,3], [2,1], [2,3], [3,1], [3,2], [3,3] ];
  var images = [img1, img2, img3, img4, img5, img6, img7, img8];
  for (var i=7; i>=0; i--) {
    var rand = Math.round(Math.random() * i); 
    var poppedPos = positions.splice(rand, 1); 
    images[i].style.gridRow = poppedPos[0][0];
    images[i].style.gridColumn = poppedPos[0][1];
  }
}

var solvePuzzle = function()  {
  movesNum = 0;
  movescell.innerHTML = movesNum; 
  [emptyRow, emptyCol] = [3, 3]; 
  img1.style.gridRow = 1;
  img1.style.gridColumn = 1;
  img2.style.gridRow = 1;
  img2.style.gridColumn = 2;
  img3.style.gridRow = 1;
  img3.style.gridColumn = 3;
  img4.style.gridRow = 2;
  img4.style.gridColumn = 1;
  img5.style.gridRow = 2;
  img5.style.gridColumn = 2;
  img6.style.gridRow = 2;
 img6.style.gridColumn = 3;
  img7.style.gridRow = 3;
 img7.style.gridColumn = 1;
  img8.style.gridRow = 3;
 img8.style.gridColumn = 2;
}


var moveImg = function() {
  thisRow = this.style.gridRow.charAt(0);
  thisCol = this.style.gridColumn.charAt(0);
  if (emptyRow == thisRow) {
    if (parseInt(thisCol)+1 == emptyCol || parseInt(thisCol)-1 == emptyCol) {
      var tempCol = thisCol; 
      this.style.gridColumn = emptyCol.toString(); 
      emptyCol = tempCol;
      movesNum++;
    }
  } else if (emptyCol == thisCol) {
    if (parseInt(thisRow)+1 == emptyRow || parseInt(thisRow)-1 == emptyRow) {
      var tempRow = thisRow;
      this.style.gridRow = emptyRow.toString(); 
      emptyRow = tempRow;
      movesNum++;
    }
  }
  movescell.innerHTML = movesNum; 

  if(emptyRow ==3&&emptyCol== 3 &&img1.style.gridRow =="1 / auto"&&img1.style.gridColumn =="1 / auto"
  &&img2.style.gridRow =="1 / auto"&&img2.style.gridColumn =="2 / auto"
  &&img3.style.gridRow =="1 / auto"&&img3.style.gridColumn =="3 / auto"
  &&img4.style.gridRow =="2 / auto"&&img4.style.gridColumn =="1 / auto"
  &&img5.style.gridRow =="2 / auto"&&img5.style.gridColumn =="2 / auto"
  &&img6.style.gridRow =="2 / auto"&&img6.style.gridColumn =="3 / auto"
  &&img7.style.gridRow =="3 / auto"&&img7.style.gridColumn =="1 / auto"
  &&img8.style.gridRow =="3 / auto"&&img8.style.gridColumn =="2 / auto"
 )
  alert("Congratulations!!!");
 
}
img1.onclick = moveImg;
img2.onclick = moveImg;
img3.onclick = moveImg;
img4.onclick = moveImg;
img5.onclick = moveImg;
img6.onclick = moveImg;
img7.onclick = moveImg;
img8.onclick = moveImg;
document.getElementById("newGame").onclick = randomizePuzzle;
document.getElementById("solveIt").onclick = solvePuzzle;

randomizePuzzle();
