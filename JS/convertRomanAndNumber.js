function convertIntoRomanViceVersa(){
    var inputValue = document.getElementById('input').value;
    if(!isNaN(inputValue)){
        var thousands = ["", "M", "MM", "MMM" ];
        var hundreds = [ "", "C", "CC", "CCC", "CD", "D","DC", "DCC", "DCCC", "CM" ];
        var tens = [ "", "X", "XX", "XXX", "XL", "L","LX", "LXX", "LXXX", "XC" ];
        var units = ["", "I", "II", "III", "IV", "V", "VI","VII", "VIII", "IX", "X" ];
        var numberOfThousands = parseInt(inputValue / 1000);
        var numberOfHundreds = parseInt((inputValue / 100) % 10);
        var numberOfTens = parseInt((inputValue / 10) % 10);
        var numberOfUnits = parseInt(inputValue % 10);
        var romanNumber = thousands[numberOfThousands] + hundreds[numberOfHundreds]+ tens[numberOfTens] + units[numberOfUnits];                  
        document.getElementById('printValue').innerHTML = romanNumber;
      }
      else  if(isNaN(inputValue))
      {
        var roman = {'I':1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}
        var val = 0;
        for (var i = inputValue.length-1; i>=0; i--) {
            var num = roman[inputValue.charAt(i)]
            if (4 * num < val){
                val -= num
            } 
            else 
            {
                val += num
            }
        }
        document.getElementById('printValue').innerHTML = val;
    }
}